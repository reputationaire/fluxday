BUG: OKRs report cannot change date range to weekly (does not seem to be due to nginx caching) => but can see weekly report at /reports/tasks

# fluxday

fluxday is a task & productivity management application ideal for fast growing startups and small companies. fluxday was developed by Foradian starting in 2014 and was a critical part of the company’s [hyper growth and success](http://www.fedena.com/history). fluxday was opensourced by [Foradian](http://foradian.com) in May 2016 to help more startups use the power of a no-nonsense productivity tracking tool.

fluxday is engineered based on the concepts of [OKR](https://en.wikipedia.org/wiki/OKR) - Objectives and Key Results, invented and made popular by John Doerr. OKRs and OKR tools are used today by many companies, including Google, LinkedIn and Twitter

## You can use fluxday for

- Managing and Tracking OKRs
- Creating, assigning and tracking tasks
- Maintaining log of time spent by employees
- Generating different types of reports, and in different formats
- Analyzing progress and productivity of your company, its departments, teams and employees
- OAuth server with filtered access to users

Visit the [official website](http://fluxday.io) for more info

> “through discipline comes freedom” - aristotle

## License

Fluxday is released under [Apache License 2.0](https://github.com/foradian/fluxday/blob/master/LICENSE)

## How to use

This is a fork of fluxday focused exclusively on Docker. For other installation methods, please refer to the [official fluxday repository](https://github.com/foradian/fluxday).

### Prerequisites

1. Install [Docker CE](https://docs.docker.com/install/)
2. If you are on Linux, install [Docker Compose](https://docs.docker.com/compose/install/)
3. Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Installation

Clone this repository (the `/fluxday` directory is just a suggestion; use whatever you want).

```sh
git clone https://gitlab.com/reputationaire/fluxday
cd fluxday
```

Copy the example configuration as `example.env` as `.env`.

```sh
cp example.env .env
```

Start the stack with:

```sh
docker-compose up -d --build
```

Wait until Docker pulls/builds the images and starts all the parts of the stack.

Fluxday can be accessed from the browser by navigating to `http://HOSTNAME:10000`, where `HOSTNAME` is `localhost` if you are running on your machine, or the remote host name/IP address if running on a server.


## Setup Nginx

Get a new SSL Certificate
```sh
~/apps/certbot-auto certonly -a webroot --webroot-path=/usr/share/nginx/html/ -d REPLACE_WITH_YOUR_DOMAIN
```

```sh
nano /etc/nginx/sites-enabled/default
```

Add
```
# Proxy requests to http://localhost:10000 for fluxday
server {

    listen 443;
    server_name REPLACE_WITH_YOUR_DOMAIN;

    default_type "text/html";

    location / {

        # TODO why does $expires map not work?
#        expires 7d;
#        add_header Pragma public;
#        add_header Cache-Control "public";

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-NginX-Proxy true;
        proxy_pass http://localhost:10000;
        proxy_ssl_session_reuse off;
        proxy_set_header Host $http_host;
        proxy_cache_bypass $http_upgrade;
        proxy_redirect off;
    }

    ssl on;
    # Use certificate and key provided by Let's Encrypt:
    ssl_certificate /etc/letsencrypt/live/REPLACE_WITH_YOUR_DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/REPLACE_WITH_YOUR_DOMAIN/privkey.pem;
    ssl_session_timeout 5m;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';

    gzip on;
    gzip_comp_level    5;
    gzip_min_length    256;
    gzip_proxied       any;
    gzip_vary          on;
    gzip_types
    application/atom+xml
    application/javascript
    application/json
    application/ld+json
    application/manifest+json
    application/rss+xml
    application/vnd.geo+json
    application/vnd.ms-fontobject
    application/x-font-ttf
    application/x-web-app-manifest+json
    application/xhtml+xml
    application/xml
    font/opentype
    image/bmp
    image/svg+xml
    image/x-icon
    text/cache-manifest
    text/css
    text/plain
    text/vcard
    text/vnd.rim.location.xloc
    text/vtt
    text/x-component
    text/x-cross-domain-policy;
    # text/html is always compressed by gzip module

}
```

Restart nginx
```sh
sudo service nginx restart ; sudo less /var/log/nginx/error.log
```


#### Initial login credentials change ASAP

Email: `admin@fluxday.io`  
Password: `password`


## Use

User must be a "Manager" to be able to use tick to complete Tasks assigned to them

Must create a Department then Team before can create Tasks

Note reports are cached (to get updated data change date range)


## Modify data

If you need to delete data older than 7 days then

```
docker exec -it fluxday-db /bin/bash
mysql -u fluxday -p                                 password in .env: fluxdaypwd
use fluxday;
delete from TABLE where ...;
```


## Screenshots

###### Dashboard - View tasks and work logs for a selected day. You can switch between month and week views.

![Dashboard](http://fluxday.io/img/screenshots/dashboard_day.jpg "Dashboard")

###### Departments and Teams - Create and manage departments. Create teams within departments and add members and leads to the teams.

![Departments and Teams](http://fluxday.io/img/screenshots/department.jpg "Departments and Teams")

###### OKR - Create and manage OKRs for a user. Set custom duration for each OKR that align to your team requirements.

![OKR](http://fluxday.io/img/screenshots/okr_view.jpg "OKR")

###### Add tasks - Create tasks for users. Enter duration, map to key result and set priority for the task.

![Add tasks](http://fluxday.io/img/screenshots/add_task.jpg "Add tasks")

###### Task view - View details of tasks like assigned users, duration and priority. You can also add subtasks from here.

![Task view](http://fluxday.io/img/screenshots/task_view.jpg "Task view")

###### Reports - Generate visual and textual reports to view performance of users. Chose between OKR, Worklogs, Tasks and Assignment based reports for an employee or employee groups.

![Reports](http://fluxday.io/img/screenshots/okr_report_hi_res.jpg "Reports")
