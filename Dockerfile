FROM phusion/baseimage:0.9.19

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

ENV LANGUAGE en_US.UTF-8

# Install os dependencies
RUN apt-add-repository ppa:brightbox/ruby-ng
RUN apt-get update && \
    apt-get install -y --option=Dpkg::Options::=--force-confdef \
    nodejs \
    ruby2.1 \
    ruby2.1-dev \
    build-essential \
    curl \
    zlib1g-dev \
    libssl-dev \
    libreadline-dev \
    libyaml-dev \
    libxml2-dev \
    libxslt-dev \
    libpq-dev \
    git python-virtualenv \
    libmysqlclient-dev \
    libmagickwand-dev \
    gnuplot \
    imagemagick-doc \
    imagemagick \
    wkhtmltopdf \
    mysql-client \
    # Clean up APT when done.
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN gem install bundler -v 1.17.3

ENV APP_HOME /share
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD Gemfile* $APP_HOME/
RUN bundle install

ADD . $APP_HOME

# Add FluxDay to runit
# https://github.com/phusion/baseimage-docker#adding_additional_daemons
RUN mkdir -p /etc/service/fluxday
COPY fluxday.sh /etc/service/fluxday/run
